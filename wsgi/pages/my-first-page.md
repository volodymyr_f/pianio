title: My first page
date: 2012-08-01

Hi! This is my first page powered by [Flask](http://flask.pocoo.org/) and [OpenShift](https://openshift.redhat.com/).

<div class="container">

    <div class="row main">
        <div class="span8 offset4" style="margin-top: 500px;">
            <!-- <p>Local mp3 file:</p> -->
            <!-- <audio src="/static/music/testfile.mp3" controls></audio> -->
            <!-- <p>Radio stream:</p> -->
            <audio src="http://ice-03.lagardere.cz:80/web-gothic-128" controls></audio>
        </div>
    </div>
</div>
